@extends('layouts.app')

@section('content')
<main id="main" class="main-site left-sidebar">

<div class="container">

    <div class="wrap-breadcrumb">
        <ul>
            <li class="item-link"><a href="#" class="link">home</a></li>
            <li class="item-link"><span>Register</span></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-12 col-md-offset-3">							
            <div class=" main-content-area">
                <div class="wrap-login-item ">
                    <div class="register-form form-item ">
                    <form method="POST" action="{{ route('register') }}">
                    @csrf
                            <fieldset class="wrap-title">
                                <h3 class="form-title">Create an account</h3>
                                <h4 class="form-subtitle">Personal infomation</h4>
                            </fieldset>									
                            <fieldset class="wrap-input">
                                <label for="frm-reg-lname">Name<span style="color:red">*</span></label>
                                <input type="text" id="frm-reg-lname" name="name" placeholder="Name">
                                @error('name')
                                    <span class=" alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset class="wrap-input">
                                <label for="frm-reg-email">Email Address<span style="color:red">*</span></label>
                                <input type="email" id="frm-reg-email" name="email" placeholder="Email address">
                                @error('email')
                                    <span class=" alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset class="wrap-title">
                                <h3 class="form-title">Login Information</h3>
                            </fieldset>
                            <fieldset class="wrap-input item-width-in-half left-item ">
                                <label for="frm-reg-pass">Password <span style="color:red">*</span></label>
                                <input type="password" id="frm-reg-pass" name="password" placeholder="Password">
                                @error('password')
                                    <span class="alert-danger" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </fieldset>
                            <fieldset class="wrap-input item-width-in-half ">
                                <label for="frm-reg-cfpass">Confirm Password <span style="color:red">*</span></label>
                                <input type="password" id="frm-reg-cfpass" name="password_confirmation" placeholder="Confirm Password">
                            </fieldset>
                            <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                                
                        </form>
                    </div>											
                </div>
            </div><!--end main products area-->		
        </div>
    </div><!--end row-->

</div><!--end container-->

</main>
@endsection
