<!DOCTYPE html>
<html>
<head>
  <title>edit category </title>
 
  <meta name="csrf-token" content="{{ csrf_token() }}">
 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://kit.fontawesome.com/939343413c.js" crossorigin="anonymous"></script>
 
</head>
<body>
 
<div class="container mt-5">
 
 <a href="/"><i class="fas fa-backward"></i>Back to home</a>
  <div class="card">
 
    <div class="card-header text-center font-weight-bold">
      <h2>Edit {{$category->name}}</h2>
    </div>
 
    <div class="card-body">
        
        <form class="form-horizontal" action="/category/{{$category->id}}" method="post" enctype="multipart/form-data">
        <table class="table table-striped">
        @csrf
        @method('PUT')
        <tr>
        <th> <label for="name">Name</label></th>
        <th><input class="form-control" type="text" id="name" name="name" placeholder="name" value="{{old('name', $category->name)}}"></th>
        @error('name')
        <th> <div class="alert alert-danger">{{ $message }}</div></th>
            @enderror
         </tr> 
        
        </table>
        <input class="btn btn-primary col-md-12 "  type="submit" value="Edit">
        </form>
 
    </div>
 
  </div>
 
</div>  
</body>
</html>