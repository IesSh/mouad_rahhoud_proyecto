@extends('layouts.app')
@section('content')

<div class="container">
	<div class="row"> 
                 <div class="col-lg-12" style="margin-top:50px;margin-bottom:50px">
                  <div class="chatbody">
                  <div class="panel panel-primary" style="border-color:#ff2832;">
                <div class="panel-heading top-bar" style="background:#ff2832;">
                    <div class="col-md-8 col-xs-8" >
                        <h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> Chat - {{$message->name}}</h3>
                    </div>
                </div>
                <div class="panel-body msg_container_base">
				<form class="form-horizontal" action="/inbox/reply/{{$message->id}}" method="get">
                               
                    <div class="row msg_container base_receive">
                        <div class="col-md-2 col-xs-2 avatar">
                            <img src="http://www.bitrebels.com/wp-content/uploads/2011/02/Original-Facebook-Geek-Profile-Avatar-1.jpg" class=" img-responsive ">
                        </div>
                        <div class="col-md-12 col-xs-10">
                            <div class="messages msg_receive">
                                <p>{{$message->comment}}</p>
                                <time datetime="2009-11-13T20:00">{{$message->created_at}}</time>
                            </div>
                        </div>
					</div>
					@if(!empty($reply))
					<div class="row msg_container base_sent">
                        <div class="col-md-10 col-xs-10">
                            <div class="messages msg_sent">
                                <p>{{$reply}}</p>
                                
                            </div>
                        </div>
                      
					</div>
					@endif
				</div>
				
                <div class="panel-footer">
                    <div class="input-group">
                        <input id="btn-input" type="text" name="reply" class="form-control input-sm chat_input" placeholder="Write your message here..." />
                        <span class="input-group-btn">
                        <button style="background:#ff2832;"  class="btn btn-primary btn-sm" id="btn-chat"><i  class="fa fa-send fa-1x" aria-hidden="true"></i></button>
                        </span>
                    </div>
                </div>
    		</div>
			</form>
                 </div>
             </div>
</div>

@endsection