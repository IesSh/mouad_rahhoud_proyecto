
@extends('layouts.app')

@section('content')
<div class="container" style="margin-top:40px;margin-bottom:70px;">
	<div class="row">
                 <div class="col-lg-12">
                 @if (\Session::has('success'))
    <div class="alert alert-success">
       
          {!! \Session::get('success') !!}
        
    </div>
@endif
                  <div class="panel panel-primary" style="border-color:#ff2832;">
		    <div class="panel-heading top-bar" style="background:#ff2832;">
                    <div class="col-md-8 col-xs-8" >
                        <h3 class="panel-title"><span ><i class="fas fa-inbox"></i></span> Messages</h3>
                    </div>
                </div>
		    <table class="table table-striped table-hover"  >
           
		        <tbody>
                @forelse($messages as $message)
                  
                <tr>
                    @if($message->statut == "unread")
		                
                        <td><a class="unread" href="/inbox/{{$message->id}}">{{$message->name}}</a> </td>
                        <td> <a class="unread"  href="/inbox/{{$message->id}}">{{$message->email}}</a></td>
                        <td> <a class="unread" href="/inbox/{{$message->id}}">{{$message->phone}}</a></td>
                        <td><a  class="unread" href="/inbox/{{$message->id}}"><i class="fas fa-envelope-square"></i>unread</a></td>
                        <td><a href="/delete/{{$message->id}}" style="color:red;" class="btn">
									<i class="fas fa-trash"></i></td> 
                       
                        @else
                        <td><a class="read" href="/inbox/{{$message->id}}">{{$message->name}}</a> </td>
                        <td> <a  class="read" href="/inbox/{{$message->id}}">{{$message->email}}</a></td>
                        <td> <a class="read" href="/inbox/{{$message->id}}">{{$message->phone}}</a></td>
                        <td><a class="read" href="/inbox/{{$message->id}}"><i class="far fa-envelope-open">read</i></a></td>
                        <td><a href="/delete/{{$message->id}}" style="color:red;" class="btn">
									<i class="fas fa-trash"></i></td> 
                       
                        @endif
                    </tr>
                    
                    @empty
<tr>
    <div class="col-md-12"><h1>No conversations found</h1></div>
</tr>
    @endforelse	
		            
		        </tbody>
            </table>
            <div class=" col-md-12">
				{{$messages->links("pagination::bootstrap-4")}}
				</div>
		</div>
                 </div>    
@endsection