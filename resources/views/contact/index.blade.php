@extends('layouts.app')

@section('content')
<main id="main" class="main-site left-sidebar">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>Contact us</span></li>
				</ul>
			</div>
			<div class="row">
				<div class=" main-content-area">
					<div class="wrap-contacts ">
						<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
							<div class="contact-box contact-form">
								<h2 class="box-title">Leave a Message</h2>
                                <form class="form-horizontal" action="/contact-us" method="post" enctype="multipart/form-data">
                                @csrf

									<label for="name">Name<span>*</span></label>
									<input type="text" value="" id="name" name="name" >
									@error('name')
       							 <p class="alert alert-danger">{{ $message }}</p>
           							 @enderror

									<label for="email">Email<span>*</span></label>
									<input type="text" value="" id="email" name="email" >
									@error('email')
       							 <p class="alert alert-danger">{{ $message }}</p>
           							 @enderror

									<label for="phone">Number Phone<span>*</span></label>
									<input type="text" value="" id="phone" name="phone" >
									@error('phone')
       							 <p class="alert alert-danger">{{ $message }}</p>
           							 @enderror
								
									<label for="comment">Comment</label>
									<textarea name="comment" id="comment"></textarea>
									@error('comment')
       							 <label class="alert alert-danger">{{ $message }}</label>
           							 @enderror

									<input type="submit" name="ok" value="Submit" >
									
								</form>
							</div>
						</div>
						<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
							<div class="contact-box contact-info">
								
								<h2 class="box-title">Contact Detail</h2>
								<div class="wrap-icon-box">

									<div class="icon-box-item">
										<i class="fa fa-envelope" aria-hidden="true"></i>
										<div class="right-info">
											<b>Email</b>
											<p>worldtechzaragoza@gmail.com</p>
										</div>
									</div>

									<div class="icon-box-item">
										<i class="fa fa-phone" aria-hidden="true"></i>
										<div class="right-info">
											<b>Phone</b>
											<p>6-66-66-66-66</p>
										</div>
									</div>

									

								</div>
							</div>
						</div>
					</div>
				</div><!--end main products area-->

			</div><!--end row-->

		</div><!--end container-->

    </main>
    @endsection