<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,400italic,700,700italic,900,900italic&amp;subset=latin,latin-ext" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open%20Sans:300,400,400italic,600,600italic,700,700italic&amp;subset=latin,latin-ext" rel="stylesheet">
	
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/animate.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/chosen.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/color-01.css')}}">
	<script src="https://kit.fontawesome.com/939343413c.js" crossorigin="anonymous"></script>




	
  
</head>
<body class="home-page home-01 ">

	<!-- mobile menu -->
    <div class="mercado-clone-wrap">
        <div class="mercado-panels-actions-wrap">
            <a class="mercado-close-btn mercado-close-panels" href="#">x</a>
        </div>
        <div class="mercado-panels"></div>
    </div>

	<!--header-->
	<header id="header" class="header header-style-1" >
		
	<div class="container">
		
					<div class="mid-section main-info-area">

					<div class="wrap-logo-top left-section">
							<a href="/" class="link-to-home"><img src={{asset('assets/images/logo1.jpeg')}} alt="'"></a>
						</div>

						<div class="wrap-search center-section">
							<div class="wrap-search-form">
								<form action="/shop" method="get" id="form-search-top">
									<input type="text" name="search" value="" placeholder="Search here...">
									<button form="form-search-top" type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
									
								</form>
							</div>
						</div>

						<div class="wrap-icon right-section">
							
							<div class="wrap-icon-section minicart">
								<a href="/cart" class="link-direction">
									<i class="fa fa-shopping-basket" aria-hidden="true"></i>
									<div class="left-info">
										<span class="index">@if(!empty(session('cart'))) {{count(session('cart'))}} items  @endif </span>
										<span class="title">CART</span>
									</div>
								</a>
							</div>
							<div class="wrap-icon-section minicart">
								<a href="/cart" class="link-direction">
								<i class="far fa-user" aria-hidden="true"></i>
									<div class="left-info">
										  @guest
                            @if (Route::has('login'))
                                <li class="nav-item" style="list-style-type:none">
                                    <a class="nav-link" style="color:black" href="{{ route('login') }}"></i>{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            @if (Route::has('register'))
                                <li class="nav-item" style="list-style-type:none">
                                    <a class="nav-link" style="color:black" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
						<li class="nav-item dropdown" style="list-style-type:none">
                                <a id="navbarDropdown" style="color:black" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
								</i> <span class="title">{{ Auth::user()->name }}</span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" style="color:black" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" >
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
									</div>
								</a>
							</div>
							
							<div class="wrap-icon-section show-up-after-1024">
								<a href="#" class="mobile-navigation">
									<span></span>
									<span></span>
									<span></span>
								</a>
							</div>
						</div>

					</div>
				</div>
				
				
				<div class="nav-section header-sticky">
					<div class="primary-nav-section">
						<div class="container">
							<ul class="nav primary clone-main-menu" id="mercado_main" data-menuname="Main menu" >
								<li class="menu-item home-icon">
									<a href="/" class="link-term mercado-item-title"><i class="fa fa-home" aria-hidden="true"></i></a>
								</li>
							
								<li class="menu-item">
									<a href="/shop" class="link-term mercado-item-title">Shop    <i class="fas fa-store"></i></a>
								</li>
								<li class="menu-item">
									<a href="/cart" class="link-term mercado-item-title">Cart     <i class="fas fa-shopping-cart">@if(!empty(session('cart') && count(session('cart'))>0 )) <span style="color:red"> +{{count(session('cart'))}}  </span> @endif</i></a>
								</li>
								<li class="menu-item">
									<a href="/checkout" class="link-term mercado-item-title">Checkout   <i class="far fa-handshake"></i></a>
								</li>
								<li class="menu-item">
									<a href="/contact-us" class="link-term mercado-item-title">Contact Us     <i class="fas fa-comments"></i></a>
								</li>	
								
								@if(!empty($user))
									@if(Auth::user()->isAdmin())
									<li class="menu-item">
									<a href="/inbox" class="link-term mercado-item-title">Inbox    <i class="fas fa-inbox">@if(!empty(session('inbox') && count(session('inbox'))>0 )) <span style="color:red"> +{{count(session('inbox'))}}  </span> @endif </i></a>
									</li>@endif
									@if(Auth::user()->isAdmin() || Auth::user()->isDelivery() )
									<li class="menu-item">
									<a href="/orders" class="link-term mercado-item-title">Orders    <i class="fas fa-truck"> @if(!empty(session('orders') && count(session('orders'))>0 )) <span style="color:red"> +{{count(session('orders'))}}  </span> @endif</i></a>
									</li>
									@endif
									@endif
									
																									
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
</div>
	</header>
	<main id="main">
            @yield('content')
        </main>


	<footer id="footer">
		<div class="wrap-footer-content footer-style-1">

			<div class="wrap-function-info">
				<div class="container">
					<ul>
						<li class="fc-info-item">
							<i class="fa fa-truck" aria-hidden="true"></i>
							<div class="wrap-left-info">
								<h4 class="fc-name">Free Shipping</h4>
								<p class="fc-desc">In Zaragoza city</p>
								
							</div>

						</li>
						<li class="fc-info-item">
							<i class="fa fa-recycle" aria-hidden="true"></i>
							<div class="wrap-left-info">
								<h4 class="fc-name">Guarantee</h4>
								<p class="fc-desc">30 Days Money Back</p>
							</div>

						</li>
						<li class="fc-info-item">
							<i class="fa fa-credit-card-alt" aria-hidden="true"></i>
							<div class="wrap-left-info">
								<h4 class="fc-name">Safe Payment</h4>
								<p class="fc-desc">Safe your online payment</p>
							</div>

						</li>
						<li class="fc-info-item">
							<i class="fa fa-life-ring" aria-hidden="true"></i>
							<div class="wrap-left-info">
								<h4 class="fc-name">Online Suport</h4>
								<p class="fc-desc">We Have Support 24/7</p>
							</div>

						</li>
					</ul>
				</div>
			</div>
			<!--End function info-->

			<div class="main-footer-content">

				<div class="container">

					<div class="row">

						<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
							<div class="wrap-footer-item">
								<h3 class="item-header">Contact Details</h3>
								<div class="item-content">
									<div class="wrap-contact-detail">
										<ul>
											
											<li>
												<i class="fa fa-phone" aria-hidden="true"></i>
												<p class="contact-txt">(+34) 6666666666</p>
											</li>
											<li>
												<i class="fa fa-envelope" aria-hidden="true"></i>
												<p class="contact-txt">worldtechzaragoza@gmail.com</p>
											</li>											
										</ul>
									</div>
								</div>
							</div>
						</div>

						
						<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12 box-twin-content ">
							
					<div class="row">

<div class="col-lg-8 col-sm-4 col-md-4 col-xs-12">
	<div class="wrap-footer-item">
		<h3 class="item-header">We Using Safe Payments:</h3>
		<div class="item-content">
			<div class="wrap-list-item wrap-gallery">
				<img src={{asset('assets/images/payment.png')}} style="width: 500px;" alt="'">
			</div>
		</div>
	</div>
</div>
						</div>

					</div>

					

						<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
							<div class="wrap-footer-item">
								<h3 class="item-header">Social network</h3>
								<div class="item-content">
									<div class="wrap-list-item social-network">
										<ul>
											<li><a href="#" class="link-to-item" title="twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
											<li><a href="#" class="link-to-item" title="facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
											<li><a href="#" class="link-to-item" title="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
											
										</ul>
									</div>
								</div>
							</div>
						</div>

					

					</div>
				</div>

			

			</div>

			
		</div>
	</footer>
	
	<script src="{{asset('assets/js/jquery-1.12.4.minb8ff.js?ver=1.12.4')}}"></script>
	<script src="{{asset('assets/js/jquery-ui-1.12.4.minb8ff.js?ver=1.12.4')}}"></script>
	<script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.flexslider.js')}}"></script>
	<script src="{{asset('assets/js/chosen.jquery.min.js')}}"></script>
	<script src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.countdown.min.js')}}"></script>
	<script src="{{asset('assets/js/jquery.sticky.js')}}"></script>
	<script src="{{asset('assets/js/functions.js')}}"></script>
	<script src="{{asset('assets/js/myFunctions.js')}}"></script>

</body>
</html>