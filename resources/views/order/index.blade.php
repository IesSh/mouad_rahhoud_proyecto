@extends('layouts.app')

@section('content')

 
<main id="main" class="main-site">

		<div class="container">

			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>login</span></li>
				</ul>
			</div>
			<div class=" main-content-area">
            @if(!empty(session('cart')))
				<div class="wrap-address-billing">
					<h3 class="box-title">Billing Address</h3>
					<form action="checkout" method="post" name="frm-billing">
                    @csrf
						<p class="row-in-form">
							<label for="fname"> Name<span>*</span></label>
							<input id="fname" type="text" name="name" value="{{old('name')}}" placeholder="Your name">
							@error('name')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
						<p class="row-in-form">
							<label for="email">Email Addreess:</label>
							<input id="email" type="email" name="email" value="{{old('email')}}" placeholder="Type your email">
							@error('email')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
						<p class="row-in-form">
							<label for="phone">Phone number<span>*</span></label>
							<input id="phone" type="number" name="phone" value="{{old('phone')}}" placeholder="10 digits format">
							@error('phone')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
						<p class="row-in-form">
							<label for="add">Address:</label>
							<input id="add" type="text" name="address" value="{{old('address')}}" placeholder="Street at apartment number">
							@error('address')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
						
						<p class="row-in-form">
							<label for="zip-code">Postcode / ZIP:</label>
							<input id="zip-code" type="number" name="zip" value="{{old('zip')}}" placeholder="Your postal code">
							@error('zip')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
						<p class="row-in-form">
							<label for="city">Town / City<span>*</span></label>
							<input id="city" type="text" name="city" value="{{old('city')}}" placeholder="City name">
							@error('city')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p>
                        <div class="summary summary-checkout">
					<div class="summary-item payment-method">
						<h4 class="title-box">Payment Method</h4>
						<div class="wrap-address-billing">
						@if (\Session::has('error'))
							<div class="alert alert-danger">
								<ul>
									<li>{!! \Session::get('error') !!}</li>
								</ul>
							</div>
						@endif
						
				
						<p class="summary-info grand-total"><span>Grand Total</span> <span class="grand-total-price">{{$total}}$</span></p>
						
						</div>
						<div class="choose-payment-methods">
                        <label class="payment-method">
								<input name="cod" id="payment-method-paypal" value="cash on delivery" type="radio">
								<span>Cash on delivery</span>
								
							</label>
							<label class="payment-method">
								<input name="cod" id="payment-method-visa" value="card" type="radio">
								<span>Credit card</span>
                                <span class="payment-desc"><p class="row-in-form">
							<label for="card-no">card number:</label>
							<input id="zip-code" type="text" name="card_no" value="{{old('card_no')}}" placeholder="card number">
							@error('card_no')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
        
						</p></span>
                                <span class="payment-desc">	<p class="row-in-form">
							<label for="exp-month">expiry month:</label>
							<input id="zip-code" type="text" name="exp_month" value="{{old('exp_month')}}" placeholder="expiry month">
							@error('exp_month')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
						</p></span>
                                <span class="payment-desc">	<p class="row-in-form">
							<label for="exp-year">expiry year:</label>
							<input id="zip-code" type="text" name="exp_year" value="{{old('exp_year')}}" placeholder="yyyy">
							@error('exp_year')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
						</p></span>
						<span class="payment-desc">		<p class="row-in-form">
							<label for="cvc">CVC:</label>
							<input id="zip-code" type="password" name="cvc" value="{{old('cvc')}}" placeholder="CVC">
							@error('cvc')
         				<label class="alert alert-danger">{{ $message }}</label>
           				 @enderror
						</p</span>
							</label>
							
						</div>
						
                        <input class="btn btn-primary col-md-12 "  type="submit" value="Place order now">
					</div>
					
				</div>
                        
                     
						
					</div>
					</form>
				</div>
              
        @else
        <div class="text-center empty_cart"> <img style="height:300px;width:400px;margin-left:30%"  src="{{asset('assets/images/cart/cart.png')}}"/>
                <h3>Your Cart Is Currently Empty</h3>
                <p>Before proceeed to checkout you must add some products to your shpping cart.<br>
            You will find a lot of interesting products on our Shop page.</p>
                <a class="link-to-shop" href="/shop">Continue Shopping  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
        @endif
				

			</div><!--end main content area-->
		</div><!--end container-->

	</main>

@endsection