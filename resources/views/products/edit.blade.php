<!DOCTYPE html>
<html>
<head>
  <title>edit product </title>
 
  <meta name="csrf-token" content="{{ csrf_token() }}">
 
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://kit.fontawesome.com/939343413c.js" crossorigin="anonymous"></script>
  <script src="{{asset('assets/js/myFunctions.js')}}"></script>
 
</head>
<body>
 
<div class="container mt-5">
 
 <a href="/shop"><i class="fas fa-backward"></i>Back to shopping page</a>
  <div class="card">
 
    <div class="card-header text-center font-weight-bold">
      <h2>Edit {{$product->name}}</h2>
    </div>
 
    <div class="card-body">
        
        <form class="form-horizontal" action="/shop/{{$product->id}}" method="post" enctype="multipart/form-data">
        <table class="table table-striped">
        @csrf
        @method('PUT')
        <tr>
        <th> <label for="name">Name</label></th>
        <th><input class="form-control" type="text" id="name" name="name" placeholder="name" value="{{old('name', $product->name)}}"></th>
        @error('name')
        <th> <div class="alert alert-danger">{{ $message }}</div></th>
            @enderror
         </tr> 
         <tr>
        <th> <label for="description">Product description</label></th>
        <th> <textarea type="text" name="description"  placeholder="description" class="form-control" style="height: calc(2.5em + 4.75rem + 2px);"
        >{{old('description', $product->description)}}</textarea></th>
        @error('description')
        <th> <div class="alert alert-danger">{{ $message }}</div></th>
            @enderror
         </tr>
         <tr>
        <th> <label for="price">Price</label></th>
        <th> <input class="form-control" type="text" name="price" placeholder="price" value="{{old('price', $product->price)}}"></th>
        @error('price')
        <th><div class="alert alert-danger">{{ $message }}</div></th>
            @enderror
              
          
         </tr>
         <tr>
        <th> <label for="quantity">Quantity</label></th>
        <th> 
        <div class="quantity-input">
									<input type="text" id="quantity" name="quantity" value="{{old('quantity', $product->quantity)}}" data-max="120" pattern="[0-9]*" >
									
									<a class="btn " onclick="increase()">+</a>
									<a class="btn" onclick="reduce()">-</a>
								</div></th>
        @error('quantity')
        <th> <div class="alert alert-danger">{{ $message }}</div></th>
            @enderror
               
            
         </tr>
         <tr>
         <th><img src="{{ asset('assets/images/products/' . $product->image) }}" style="width:100%;height:200px"/></th>
        <th> <input class="form-control" type="file" name="image" placeholder="Choose image" id="image" value="ok"></th>
       
                
           
         </tr> 
         <tr>
<th>Category </th>
<th>
<select name="category_id" id="category_id" class="form-control form_cont" required>

<option selected="true" disabled="disabled">categories</option>

@foreach($categories as $category)
<option value="{{ $category->id }}" {{$category->id == $product->category_id  ? 'selected' : ''}}>{{ $category->name }}</option>
@endforeach
</select>
</th>
</tr>
        </table>
        <input class="btn btn-primary col-md-12 "  type="submit" value="Edit">
        </form>
 
    </div>
 
  </div>
 
</div>  
</body>
</html>