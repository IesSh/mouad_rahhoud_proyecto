@extends('layouts.app')

@section('content')
<main id="main" class="main-site">

		<div class="container">
        @if (\Session::has('success'))
    <div class="alert alert-success">
       
          {!! \Session::get('success') !!}
        
    </div>
@endif
@if (\Session::has('error'))
    <div class="alert alert-danger">
       
            {!! \Session::get('error') !!}
       
    </div>
@endif
			<div class="wrap-breadcrumb">
				<ul>
					<li class="item-link"><a href="#" class="link">home</a></li>
					<li class="item-link"><span>Cart</span></li>
				</ul>
			</div>
			<div class=" main-content-area">
            @if(!empty(session('cart')))
				<div class="wrap-iten-in-cart">
					<h3 class="box-title">Products Name</h3>
					<ul class="products-cart">
                    <li class="pr-cart-item">
							<div class="product-image">
                            <p class="price" style="font-size:16px;"><b>image</b></p>
							</div>
							<div class="product-name">
                            <p class="price" style="font-size:16px;"><b>Product name</b></p>
							</div>
							<div class="price-field produtc-price"><p class="price">Price</p></div>
							<div class="quantity">
                            <p class="price" style="font-size:16px;"><b>Quantity</b></p>
							</div>
							<div class="price-field sub-total"><p class="price">Total</p></div>
							<div class="delete">
								<a href="#" class="btn btn-delete" title="">
									<span>Delete from your cart</span>
									
								</a>
							</div>
                        </li>
                        
                        @foreach(session('cart') as $id=>$product)
						<li class="pr-cart-item">
							<div class="product-image">
								<figure><img src="{{ asset('assets/images/products/' . $product['image']) }}" /></figure>
							</div>
							<div class="product-name">
								<a class="link-to-product" href="#">{{$product['name']}}</a>
							</div>
							<div class="price-field produtc-price"><p class="price">{{$product['price']}}$</p></div>
							<div class="quantity">
                            <div style="font-size:16px;">
                                    <p><b> {{$product['quantity']}} </b>
                                    <a style="color:#222222;" href="reduce/{{$product['id']}}" class="btn"><i class="fas fa-minus-circle"></i></a>
                                    <a style="color:#222222;" href="increase/{{$product['id']}}" class="btn" ><i class="fas fa-plus-circle"></i></a></p>	
                            </div>
							</div>
							<div class="price-field sub-total"><p class="price">{{$product['price'] * $product['quantity']}} $</p></div>
							<div class="delete">
								<a href="/remove/{{$product['id']}}" class="btn">
									<i class="fa fa-times-circle" aria-hidden="true"></i>
								</a>
                            </div>
                          
                        </li>
    @endforeach		
  				
					</ul>
				</div>

				<div class="summary">
					<div class="order-summary">
						<p class="summary-info total-info "><span class="title">Total</span><b class="index">{{$total}}$</b></p>
					</div>
					<div class="checkout-info">
						
						<a class="btn btn-checkout" href="checkout">Check out</a>
						<a class="link-to-shop" href="/shop">Continue Shopping<i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a>
					</div>
					<div class="update-clear">
						<a class="btn btn-clear" href="/clear">Clear Shopping Cart</a>
					</div>
                </div>
                @else 
                <div class="text-center empty_cart"> 
				<img style="height:300px;width:400px;margin-left:30%"  src="{{asset('assets/images/cart/cart.png')}}"/>
                <h3>Your Cart Is Currently Empty</h3>
                <p>Before proceeed to checkout you must add some products to your shpping cart.<br>
            You will find a lot of interesting products on our Shop page.</p>
                <a class="link-to-shop" href="/shop">Continue Shopping  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></a></div>
               
               
                @endif		


			</div><!--end main content area-->
		</div><!--end container-->

    </main>
    <style>
       
    </style>
    @endsection