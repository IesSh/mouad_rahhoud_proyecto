<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BasketController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\OrderDetailController;
use App\Http\Controllers\MController;
use App\Http\Controllers\ChatController;
use App\Http\Controllers\InboxController;

use Illuminate\Support\Facades\Auth;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|#Florin
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*
|--------------------------------------------------------------------------

Route::resource('users',UserController::class);


/*|--------------------------------------------------------------------------*/

/* Rutas comunes --------------------------------------------------------------*/
Route::resource('users', UserController::class); //->middleware('auth');
/*-----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------*/


Route::resource('shop', ProductController::class)/*->middleware('auth')*/;
Route::resource('category', CategoryController::class)->except(['show','index']);
Route::resource('/', ProductController::class);
Route::resource('checkout',OrderController::class)->middleware('auth');
Route::resource('/cart', BasketController::class);
Route::get('reduce/{id}', [BasketController::class,'reduce']);
Route::get('clear', [BasketController::class,'clear']);
Route::get('increase/{id}', [BasketController::class,'increase']);
Route::get('remove/{id}', [BasketController::class,'remove']);
Route::get('shop/delete/{id}', [ProductController::class,'delete']);
Route::get('delete/{id}', [InboxController::class,'delete']);
Route::get('destroy', [BasketController::class,'destroy']);
Route::get('/shop/sortbyCateg/{name}/{id}', [ProductController::class,'sortbyCateg']);
Route::get('/inbox/reply/{id}', [InboxController::class,'reply']);
Route::get('/shop/add_to_cart/{id}', [BasketController::class,'add_to_cart']);
Route::get('/send-email',[MController::class,'sendEmail']);
Route::resource('/contact-us', ChatController::class);
Route::resource('/inbox', InboxController::class);
Route::resource('/orders', OrderDetailController::class);
Route::get('/orders/{id}/delivered', [OrderDetailController::class,'delivered']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


