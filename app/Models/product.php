<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class product extends Model
{
    use HasFactory;
    protected $fillable = ["name", "image", "price", "discount","quantity","description"];
    public function category(){
        return $this->HasOne(Category::class);
    }
    public function order_detail(){
        return $this->hasMany(order_detail::class);

    }
    
}
