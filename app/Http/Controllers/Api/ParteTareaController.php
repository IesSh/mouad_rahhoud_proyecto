<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\ParteTarea;
use Auth;
use PhpParser\Node\Stmt\Return_;
class ParteTareaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index()
    {  
         $partes = ParteTarea::all();
        return response()->json([
            'status' => 200,
            'data' => $partes
        ], 200);
  
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {    
        $user=Auth::user();
        if ($user->can('create', ParteTarea::class)) {
        $request->user_id=$user->id;
        $rules = [
            'date' => 'required|date_format:Y-m-d'
        ];        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }
        
        $parte = ParteTarea::create([
            'user_id' => $request->user_id,
            'date' => $request->date
        ]);
        return response()->json([
            'status' => 'ok',
            'data' => $parte
        ], 201);
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);

    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
        $parte = ParteTarea::find($id);
        $this->check404($parte);
        return response()->json([
            'status' => 'ok',
            'data' => $parte
        ], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function patch(Request $request, $id)
    {
        $user = \Auth::user();
        if ($user->can('update', ParteTarea::class)) {
        if ($request->date) {
            //si no pasa validación 422
            $rules = [
                'date' => 'required|date_format:Y-m-d'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'errors' => $validator->errors()
                ], 422);                
            }
        }

        //todo ok 200
        $parte->fill($request->all());
        $parte->save();
        return response()->json([
            'status' => 'ok',
            'data' => $parte
        ], 200);
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);      
    }        

    }
       
    

    public function validarCampo($campo, $regla)
    {
        # code...
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = \Auth::user();
        if ($user->can('update', ParteTarea::class)) {
        if ($request->method() == 'PATCH') {
            return $this->patch($request, $id);
        }
        $parte = ParteTarea::find($id);     
        $this->check404($parte);

        //si no pasa validación 422
        $rules = [
            'date' => 'required|date_format:Y-m-d' 
        ];        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }

        //todo ok 200
        $parte->fill($request->all());
        $parte->save();
        return response()->json([
            'status' => 'ok',
            'data' => $parte
        ], 200);      
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);      
    }          
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    { $user = \Auth::user();
        if ($user->can('delete', ParteTarea::class)) {
        $parte = ParteTarea::find($id);
        $this->check404($parte);

        try {
            //status 204: No content
            $parte->delete();
            return response()->json([
                'Sin contenido'
            ], 204);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Borrado fallido. Conflicto',
                'error_message' => $th->getMessage()
            ], 409);
        }
    }
    else{
        return response()->json([
            'status' => 403,
            'data'=>'no autorizado'
        ], 403);
    }
    }

    //DRY. Don't Repeat Yourself
    public function check404($parte)
    {
        if (!$parte) {
            response()->json([
                'status' => 404,
                'message' => 'No se ha encontrado parte con ese id'
            ], 404)->send();
            die();
        }
    }    
}

