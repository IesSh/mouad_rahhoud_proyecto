<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\LineaParte;
use Auth;
use DB;
use PhpParser\Node\Stmt\Return_;
class LineaParteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
       
         $lineas = LineaParte::all();
        return response()->json([
            'status' => 200,
            'data' => $lineas
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      $user=Auth::user();
        if ($user->can('create', LineaParte::class)) {
        $rules = [
            'tarea_id' => 'required',
            'comentario'=>'required'
        ];        
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }
        $linea = LineaParte::create($request->all());
        return response()->json([
            'status' => 'ok',
            'data' => $linea
        ], 201);
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);

    }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $linea=DB::table('lineas_parte')->where('orden',$id)->first();
        $this->check404($linea);
        return response()->json([
            'status' => 'ok',
            'data' => $linea
        ], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }


    public function patch(Request $request, $id)
    {
        $user = \Auth::user();
        if ($user->can('update', LineaParte::class)) {     
        if ($request->tarea_id) {
            //si no pasa validación 422
            $rules = [
                'tarea_id' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'errors' => $validator->errors()
                ], 422);                
            }
        }
        if ($request->comentario) {
            //si no pasa validación 422
            $rules = [
                'comentario' => 'required'
            ];
            $validator = Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'errors' => $validator->errors()
                ], 422);                
            }
        }

        //todo ok 200
        $linea->fill($request->all());
        $linea->save();
        return response()->json([
            'status' => 'ok',
            'data' => $linea
        ], 200);
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403); 
    }    
    }

    public function validarCampo($campo, $regla)
    {
        # code...
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   $user = \Auth::user();
        if ($user->can('update', LineaParte::class)) {     
        if ($request->method() == 'PATCH') {
            return $this->patch($request, $id);
        }
        $linea=DB::table('lineas_parte')->where('orden',$id)->first();  
        $this->check404($linea);

        //si no pasa validación 422
        $rules = [
            'tarea_id' => 'required',
            'comentario' => 'required'
        ];        
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'errors' => $validator->errors()
            ], 422);                
        }

        //todo ok 200
        $linea->fill($request->all());
        $linea->save();
        return response()->json([
            'status' => 'ok',
            'data' => $linea
        ], 200);    
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);     
    }            
}

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = \Auth::user();
        if ($user->can('update', LineaParte::class)) {     
        $linea=DB::table('lineas_parte')->where('orden',$id)->first();
        $this->check404($linea);

        try {
            //status 204: No content
            $linea->delete();
            return response()->json([
                'Sin contenido'
            ], 204);
        } catch (\Throwable $th) {
            return response()->json([
                'status' => 'error',
                'message' => 'Borrado fallido. Conflicto',
                'error_message' => $th->getMessage()
            ], 409);
        }
    }
    else{
        return response()->json([
            'status' => 403,
            'data' => 'no autorizado'
        ], 403);     
    }            
    }

    //DRY. Don't Repeat Yourself
    public function check404($linea)
    {
        if (!$linea) {
            response()->json([
                'status' => 404,
                'message' => 'No se ha encontrado linea con ese id'
            ], 404)->send();
            die();
        }
    }    
}

