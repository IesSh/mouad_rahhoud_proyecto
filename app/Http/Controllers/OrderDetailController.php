<?php

namespace App\Http\Controllers;

use App\Models\order_detail;
use Illuminate\Http\Request;
use App\Models\order;
use Auth;
use Carbon;

class OrderDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAny', order::class); 
        $user=Auth::user();
        $query=order::query();
        $query->orderBy('created_at', 'desc')->get();
        $orders=$query->paginate(9);
        return view('order.orders',['orders'=>$orders,'user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\order_detail  $order_detail
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user=Auth::user();
        $order=order::find($id);
        $query=order_detail::query();
        $query->where('order_id','like',$order->id);
        $details=$query->paginate(9);
        return view('order.details',['order'=>$order,'details'=>$details,'user'=>$user]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\order_detail  $order_detail
     * @return \Illuminate\Http\Response
     */
    public function edit(order_detail $order_detail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\order_detail  $order_detail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
    public function delivered( $id)
    {
        $order=order::find($id);
        $order->status="delivred";
        $order->save();
        return redirect()->back()->with('success','product removed from  cart with successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\order_detail  $order_detail
     * @return \Illuminate\Http\Response
     */
    public function destroy(order_detail $order_detail)
    {
        //
    }
}
