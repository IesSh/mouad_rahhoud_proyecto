<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\category;
use Auth;
use App\Models\chat;
use App\Models\order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       /* $this->middleware('auth');*/
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        
        dd("zebi");$query=Chat::query();
        $query->where('statut','like',"unread")->get();
        $user=Auth::user();
        $categories=Category::all();
        $inbox=session()->get('inbox');
        $inbox=$query->paginate(8);
        $query=order::query();
        $query->where('status','like',"in progress")->get();
        $orders==session()->get('orders');
        session()->put('orders',$orders);
       session()->put('inbox',$inbox); 
        return view('welcome',['categories'=>$categories,'user'=>$user]);
    }
}
