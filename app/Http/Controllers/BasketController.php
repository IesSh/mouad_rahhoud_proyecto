<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\product;
use session;
use Auth;

class BasketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        $user=Auth::user();

        if(!empty(session('cart')))
        { $cart=session()->get('cart');
            $total=0;
            foreach($cart as $product){
                $total=$total+($product['price']*$product['quantity']);
            }
           
            return view('cart',["total"=>$total,"user"=>$user]);
        }
            else{
                return view('cart',["user"=>$user]);
            }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }
    public function clear()
    {
        session()->forget('cart');
        return redirect()->back()->with('success',' cart cleared with successfully');
    }
    public function remove($id)
    {
        $product=Product::find($id);
        $cart=session()->get('cart');
        unset($cart[$product->id]);
        session()->put('cart',$cart);
        return redirect()->back()->with('success','product removed from  cart with successfully');
    }
    function add_to_cart($id,Request $request){
        $product=Product::find($id);
        if(!empty($request->quantity))
        {
            $quantity=$request->quantity;
            if($request->quantity>$product->quantity)
            {
                return redirect()->back()->with('error','We have only'.' '.$product->quantity.' in stock ');
            }
        }
        else{
            $quantity=1;
        }
       
       $cart=session()->get('cart');
       $cart[$id]=[
        "id"=>$product->id,   
        "name"=>$product->name,
       "quantity"=>$quantity,
       "stock"=>$product->quantity,
       "price"=>$product->price,
       "image"=>$product->image,
       "description"=>$product->description,
        "category_id"=>$product->category_id];
       session()->put('cart',$cart); 
       return redirect()->back()->with('success','product added to cart with successfully');
    }
    function reduce($id){
        $cart=session()->get('cart');
        if( $cart[$id]['quantity']>1){
            $cart[$id]['quantity']=$cart[$id]['quantity']-1 ;
            session()->put('cart',$cart);
        }
    
       
        return redirect()->back()->with('success','cart updated with successfully');
    }
    function increase($id){
        $product=Product::find($id);
        $cart=session()->get('cart');
        if( $cart[$id]['stock']>$cart[$id]['quantity']){
        $cart[$id]['quantity']=$cart[$id]['quantity']+1 ;
        session()->put('cart',$cart);
        return redirect()->back()->with('success','cart updated with successfully');
        }
        else{
            return redirect()->back()->with('error','We have only'.' '.$product->quantity.' in stock ');
        }
       
        
    }
}
