<?php

namespace App\Http\Controllers;
use Exception;

use App\Models\order;
use App\Models\order_detail;
use App\Models\product;
use Illuminate\Http\Request;
use App\Mail\Mails;
use App\Mail\delivery;
Use Mail;
use Auth;
use Stripe;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        if(!empty(session('cart')))
        { $cart=session()->get('cart');
            $total=0;
            foreach($cart as $product){
                $total=$total+($product['price']*$product['quantity']);
                
            }
            return view('order.index',["total"=>$total,"user"=>$user]);
        }
            else{
                return view('order.index',["user"=>$user]);
            }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=Auth::user();
        $payed="cash on delivery";
        $cart=session()->get('cart');
        $total=0;
        foreach($cart as $product){
            $total=$total+($product['price']*$product['quantity']);
        }
     if($request->cod=='cash on delivery')
     {
        $rules=['name'=>'required','email'=>'required|email','phone'=>'required|numeric','address'=>'required',
        'cod' => 'required','zip'=>'required|numeric','city'=>'required'];
         $payed="cash on delivery";
         $request->validate($rules);
     }
     else{
        $rules=['name'=>'required','email'=>'required|email','phone'=>'required|numeric','address'=>'required',
        'cod' => 'required','zip'=>'required|numeric','card_no'=>'required|numeric','exp_month'=>'required|numeric','exp_year'=>'required|numeric','cvc'=>'required|numeric','city'=>'required'];
        $request->validate($rules);
         $stripe=Stripe::make(env('STRIPE_KEY'));
        try{
            $token=$stripe->tokens()->create([
                'card'=>[
                    'number'=>$request->card_no,
                    'exp_month'=>$request->exp_month,
                    'exp_year'=>$request->exp_year,
                    'cvc'=>$request->cvc
                ]
            ]);
            if(!isset($token['id']))
            {
                session()->flash('stripe_error','the stripe token wwas not generated correctly');

            }
            $custumer=$stripe->customers()->create([
                'name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'address'=>[
                    'line1'=>$request->add,
                    'city'=>$request->city,
                    
                ],
                'source'=>$token['id']
            ]);
            $charge=$stripe->charges()->create([
                'customer'=>$custumer['id'],
                'currency'=>'USD',
                'amount'=>$total,
                'description'=>'payement'
            ]);
            $payed="payed with credit card";
            
        }
        catch(Exception $e)
        {
           
            return back()->with('error',$e->getMessage());
        }
     }
  
   
     $order = new order;
     $order->user_id =$user->id;
     $order->name =$request->name;
     $order->email = $request->email;
     $order->phone=$request->phone;
     $order->total=$total;
     $order->adress=$request->address;
     $order->payed=$payed;
     $order->city=$request->city;
     $order->zip=$request->zip;
     $order->status="in progress";
     $order->save();
     
     foreach($cart as $products){
        $order_detail = new order_detail;
        $order_detail->order_id =$order->id;
        $order_detail->product_id =$products['id'];
        $order_detail->price = $products['price']*$products['quantity'];
        $order_detail->quantity=$products['quantity'];
        $order_detail->save();
        $product=Product::find($products['id']);
        $product->quantity=($product->quantity - $products['quantity']);
        $product->save();
     }
     
    $order_detail=[
        'order'=>$order->id,
        'total'=>$total,
        'link'=>'http://127.0.0.1:8000/orders/'.$order->id
    ];
    

    Mail::to("repartidorworldtech@gmail.com")->send(new delivery($order_detail));
     session()->forget('cart');
     return view('thanks');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(order $order)
    {
        //
    }
}
