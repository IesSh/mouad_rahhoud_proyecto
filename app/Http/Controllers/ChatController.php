<?php

namespace App\Http\Controllers;

use App\Models\chat;
use Illuminate\Http\Request;
use Auth;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=Auth::user();
        return view('contact.index',["user"=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=['name'=>'required','email'=>'required|email','phone'=>'required|numeric','comment'=>'required'];
        $request->validate($rules);
        $statut="unread";
        $chat = new Chat;
           $chat->name =$request->name;
           $chat->statut = $statut;
           $chat->email=$request->email;
           $chat->phone=$request->phone;
           $chat->comment=$request->comment;
           $chat->save();

            $inbox=session()->get('inbox');
           $inbox[$chat->id]=[
            "id"=>$chat->id,   
            "name"=>$request->name,
           "comment"=>$request->comment,
           "phone"=>$request->phone,
           "email"=>$request->email,
           "status"=>$request->status,];
           session()->put('inbox',$inbox);
           return view('contact.thanks');
          
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function show(chat $chat)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function edit(chat $chat)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, chat $chat)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\chat  $chat
     * @return \Illuminate\Http\Response
     */
    public function destroy(chat $chat)
    {
        //
    }
}
